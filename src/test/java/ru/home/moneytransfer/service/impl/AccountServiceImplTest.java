package ru.home.moneytransfer.service.impl;

import com.google.inject.Inject;
import org.junit.Test;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.model.TestData;
import ru.home.moneytransfer.service.AccountService;
import ru.home.moneytransfer.service.TestServiceBaseModule;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ru.home.moneytransfer.TestUtil.assertMatchAccount;
import static ru.home.moneytransfer.model.TestData.ACCOUNT_1;

/**
 * Account service test
 * @author Pospekhov S
 */

public class AccountServiceImplTest extends TestServiceBaseModule {

    @Inject
    private AccountService accountService;

    @Test
    public void save() throws Exception {
        Account expected = new Account("name", new BigDecimal("100"));
        Account actual = accountService.save(expected);
        expected.setId(actual.getId());

        assertMatchAccount(actual, expected);
    }

    @Test
    public void getAll() throws Exception {
        List<Account> actual = accountService.getAll();
        List<Account> expected = Stream.of(ACCOUNT_1, TestData.ACCOUNT_2).collect(Collectors.toList());

        assertMatchAccount(actual, expected);
    }

    @Test
    public void get() throws Exception {
        Account actual = accountService.get(ACCOUNT_1.getId());

        assertMatchAccount(actual, ACCOUNT_1);
    }

}