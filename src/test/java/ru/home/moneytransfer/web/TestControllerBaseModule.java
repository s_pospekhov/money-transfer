package ru.home.moneytransfer.web;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.After;
import org.junit.Before;
import ru.home.moneytransfer.repository.RepositoryModule;
import ru.home.moneytransfer.repository.inmemory.TestStoreObject;
import ru.home.moneytransfer.service.ServiceModule;
import ru.home.moneytransfer.util.PropertiesModule;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Base class for controller test. Init guice injector. Start jetty server.
 * @author Pospekhov S
 */

public class TestControllerBaseModule {
    protected Server server;
    protected Client client;

    private Injector injector = Guice.createInjector(new ServiceModule(), new AppServletModule(), new RepositoryModule(), new PropertiesModule()
    );

    @Before
    public void setUp() throws Exception {
        injector.injectMembers(this);

        int port = 8089;
        server = new Server(port);

        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);

        servletContextHandler.addEventListener(new GuiceServletContextListener() {
            @Override
            protected Injector getInjector() {
                return injector;
            }
        });
        servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
        servletContextHandler.addServlet(DefaultServlet.class, "/");

        server.setHandler(servletContextHandler);

        TestStoreObject.clear();
        TestStoreObject.initForTest();

        server.start();

        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        client = Client.create(clientConfig);
    }

    @After
    public void setDown() throws Exception{
        server.stop();
    }
}
