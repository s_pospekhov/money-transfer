package ru.home.moneytransfer.model;

import java.math.BigDecimal;

/**
 *  Model class for single transfer transaction
 *
 *  @author Pospekhov S
 */

public class Transaction {
    private Long id;

    private String message;

    private BigDecimal amount;

    private Long fromAccountId;

    private Long toAccountId;

    private Boolean isSuccess;

    public Transaction() {
    }

    public Transaction(String message, BigDecimal amount, Long from, Long to) {
        this.message = message;
        this.amount = amount;
        this.fromAccountId = from;
        this.toAccountId = to;
    }

    public Transaction(Long id, String message, BigDecimal amount, Long from, Long to, Boolean isSuccess) {
        this.id = id;
        this.message = message;
        this.amount = amount;
        this.fromAccountId = from;
        this.toAccountId = to;
        this.isSuccess = isSuccess;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(Long fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public Long getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(Long toAccountId) {
        this.toAccountId = toAccountId;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", amount=" + amount +
                ", from=" + fromAccountId +
                ", to=" + toAccountId +
                ", isSuccess=" + isSuccess +
                '}';
    }
}
