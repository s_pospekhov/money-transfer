package ru.home.moneytransfer.service;

import ru.home.moneytransfer.model.Account;

import java.util.List;

/**
 *  Service for account
 *
 *  @author Pospekhov S
 */

public interface AccountService {
    /**
     * Save object in repository
     * @param account object for save
     * @return Saved account object
     */
    Account save(Account account);

    /**
     * Get object
     * @param id account unique id
     * @return Account object
     */
    Account get(Long id);

    /**
     * Get all objects from repository
     * @return List account objects
     */
    List<Account> getAll();
}
