package ru.home.moneytransfer.model;

import java.math.BigDecimal;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for test data
 * @author Pospekhov S
 */

public class TestData {

    public static final Account ACCOUNT_1;
    public static final Account ACCOUNT_2;

    static {
        ACCOUNT_1 = new Account();
        ACCOUNT_1.setId(1001L);
        ACCOUNT_1.setName("account name 1");
        ACCOUNT_1.setAmount(new BigDecimal("1000.00"));

        ACCOUNT_2 = new Account();
        ACCOUNT_2.setId(1002L);
        ACCOUNT_2.setName("account name 2");
        ACCOUNT_2.setAmount(new BigDecimal("2000.00"));
    }

    public static final Transaction TRANSACTION_1;
    public static final Transaction TRANSACTION_2;

    static {
        TRANSACTION_1 = new Transaction("transfer from 1 to 2", new BigDecimal("500.00"), ACCOUNT_1.getId(), ACCOUNT_2.getId());
        TRANSACTION_1.setId(1001L);
        TRANSACTION_1.setSuccess(true);

        TRANSACTION_2 = new Transaction("transfer from 2 to 1", new BigDecimal("100.00"), ACCOUNT_2.getId(), ACCOUNT_1.getId());
        TRANSACTION_2.setId(1002L);
        TRANSACTION_2.setSuccess(true);

        ACCOUNT_1.setTransactionsFrom(Stream.of(TRANSACTION_1).collect(Collectors.toList()));
        ACCOUNT_1.setTransactionsTo(Stream.of(TRANSACTION_2).collect(Collectors.toList()));
        ACCOUNT_2.setTransactionsFrom(Stream.of(TRANSACTION_2).collect(Collectors.toList()));
        ACCOUNT_2.setTransactionsTo(Stream.of(TRANSACTION_1).collect(Collectors.toList()));
    }


}
