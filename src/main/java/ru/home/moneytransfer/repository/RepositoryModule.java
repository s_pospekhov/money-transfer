package ru.home.moneytransfer.repository;

import com.google.inject.AbstractModule;
import ru.home.moneytransfer.repository.inmemory.AccountRepositoryLocal;
import ru.home.moneytransfer.repository.inmemory.TransactionalRepositoryLocal;

/**
 * Repository classes binding
 * @author Pospekhov S
 */

public class RepositoryModule extends AbstractModule {
    protected void configure() {
        bind(AccountRepository.class).to(AccountRepositoryLocal.class);
        bind(TransactionRepository.class).to(TransactionalRepositoryLocal.class);
    }
}
