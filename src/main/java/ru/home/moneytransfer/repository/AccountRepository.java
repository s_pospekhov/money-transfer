package ru.home.moneytransfer.repository;

import ru.home.moneytransfer.model.Account;

import java.util.List;

/**
 *  Repository for account
 *
 *  @author Pospekhov S
 */

public interface AccountRepository {

    /**
     * Get object
     * @param id account unique id
     * @return Account object
     */
    Account get(Long id);

    /**
     * Get object with lock. Object unlock after save.
     * @param id account unique id
     * @return Object for account
     */
    Account getWithLock(Long id);

    /**
     * Save object in repository
     * @param account object for save
     * @return Saved account object
     */
    Account save(Account account);

    /**
     * Get all objects from repository
     * @return List account objects
     */
    List<Account> getAll();

}
