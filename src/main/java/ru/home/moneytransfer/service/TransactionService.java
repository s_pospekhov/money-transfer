package ru.home.moneytransfer.service;

import ru.home.moneytransfer.model.Transaction;

import java.util.List;

/**
 *  Service for transaction
 *
 *  @author Pospekhov S
 */

public interface TransactionService {
    /**
     * Get object
     * @param id transaction unique id
     * @return Transaction object
     */
    Transaction get(Long id);

    /**
     * Save object in repository. Update account amount. Add transaction to account.
     * @param transaction Object for save
     * @return Saved transaction object
     * @throws ru.home.moneytransfer.util.exception.NotEnoughMoney when not enough money for transaction
     */
    Transaction save(Transaction transaction);

    /**
     * Get all objects from repository
     * @return List transaction objects
     */
    List<Transaction> getAll();
}
