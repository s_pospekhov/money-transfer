package ru.home.moneytransfer.util.exception;

import org.eclipse.jetty.http.HttpStatus;

/**
 *  Custom application exception
 *
 *  @author Pospekhov S
 */

public class ApplicationException extends RuntimeException {

    private final String msg;
    private final HttpStatus.Code httpStatus;

    public ApplicationException(String msg, HttpStatus.Code httpStatus) {
        this.msg = msg;
        this.httpStatus = httpStatus;
    }

    public String getMsg() {
        return msg;
    }

    public HttpStatus.Code getHttpStatus() {
        return httpStatus;
    }
}
