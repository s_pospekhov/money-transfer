package ru.home.moneytransfer.repository.inmemory;

import javafx.util.Pair;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.model.Transaction;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 *  Implementation for local store
 *
 *  @author Pospekhov S
 */

class StoreObjects {
    static Map<Long, Pair<Account,Semaphore>> ACCOUNTS = new HashMap<>();
    static Map<Long, Transaction> TRANSACTIONS = new HashMap<>();

    static AtomicLong ACCOUNT_ID = new AtomicLong(1L);
    static AtomicLong TRANSACTION_ID = new AtomicLong(1L);

    static void setACCOUNTS(Map<Long, Pair<Account, Semaphore>> ACCOUNTS) {
        StoreObjects.ACCOUNTS = ACCOUNTS;
    }

    static void setTRANSACTIONS(Map<Long, Transaction> TRANSACTIONS) {
        StoreObjects.TRANSACTIONS = TRANSACTIONS;
    }

    static void setAccountId(AtomicLong accountId) {
        ACCOUNT_ID = accountId;
    }

    static void setTransactionId(AtomicLong transactionId) {
        TRANSACTION_ID = transactionId;
    }

    static Account getClone(Account account) {
        Account result = new Account();
        result.setId(account.getId());
        result.setName(account.getName());
        result.setAmount(account.getAmount());
        result.setTransactionsTo(account.getTransactionsTo().stream().map(StoreObjects::getClone).collect(Collectors.toList()));
        result.setTransactionsFrom(account.getTransactionsFrom().stream().map(StoreObjects::getClone).collect(Collectors.toList()));
        return result;
    }

    static Transaction getClone(Transaction transaction) {
        return new Transaction(transaction.getId(), transaction.getMessage(), transaction.getAmount(), transaction.getFromAccountId(), transaction.getToAccountId(), transaction.getSuccess());
    }
}
