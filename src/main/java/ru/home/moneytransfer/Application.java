package ru.home.moneytransfer;

import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Main application class
 * Create server and servletContextHandler for it
 * @author Pospekhov S
 */

public class Application {

    public static void main(String[] args) throws Exception{
        Server server = new Server(8081);

        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);

        servletContextHandler.addEventListener(new InitializeGuiceModulesContextListener());
        servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
        servletContextHandler.addServlet(DefaultServlet.class, "/");

        server.setHandler(servletContextHandler);

        server.start();

        server.join();
    }
}
