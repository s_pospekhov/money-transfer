package ru.home.moneytransfer.web.controller;

import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import ru.home.moneytransfer.model.Transaction;
import ru.home.moneytransfer.service.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Controller class for transaction
 * @author Pospekhov S
 */

@Path("/transaction")
@RequestScoped
public class TransactionController {
    private final TransactionService transactionService;

    @Inject
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Transaction save(Transaction transaction) {
        return transactionService.save(transaction);
    }
}
