package ru.home.moneytransfer.service.impl;

import com.google.inject.Inject;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.repository.AccountRepository;
import ru.home.moneytransfer.service.AccountService;

import java.util.List;

/**
 *  Implementation for service for account
 *
 *  @author Pospekhov S
 */

public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    @Inject
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    public Account get(Long id) {
        return accountRepository.get(id);
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.getAll();
    }
}
