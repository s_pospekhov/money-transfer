package ru.home.moneytransfer.web.controller;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;
import ru.home.moneytransfer.model.Transaction;
import ru.home.moneytransfer.web.TestControllerBaseModule;

import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.home.moneytransfer.TestUtil.assertMatchTransaction;
import static ru.home.moneytransfer.model.TestData.ACCOUNT_1;
import static ru.home.moneytransfer.model.TestData.ACCOUNT_2;

/**
 * Class for transaction controller test
 * @author Pospekhov S
 */

public class TransactionControllerTest extends TestControllerBaseModule {


    @Test
    public void save() throws Exception {
        WebResource webResource = client
                .resource(server.getURI())
                .path("transaction");

        Map<String,Object> postBody = new HashMap<>();
        postBody.put("message", "new transaction");
        postBody.put("amount", 100);
        postBody.put("fromAccountId", 1001);
        postBody.put("toAccountId", 1002);

        Transaction expected = new Transaction("new transaction", new BigDecimal("100"), ACCOUNT_1.getId(), ACCOUNT_2.getId());
        ClientResponse response = webResource
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, postBody);

        assertThat(response.getStatus()).isEqualTo(200);

        Transaction actual = response.getEntity(new GenericType<Transaction>() {});
        expected.setId(actual.getId());
        expected.setSuccess(actual.getSuccess());

        assertMatchTransaction(actual, expected);
    }

}