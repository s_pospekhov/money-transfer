package ru.home.moneytransfer.web.controller;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.web.TestControllerBaseModule;

import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.home.moneytransfer.TestUtil.assertMatchAccount;
import static ru.home.moneytransfer.model.TestData.ACCOUNT_1;

/**
 * Class for account controller test
 * @author Pospekhov S
 */

public class AccountControllerTest extends TestControllerBaseModule {

    @Test
    public void get() throws Exception {

        WebResource webResource = client
                .resource(server.getURI())
                .path("account");

        ClientResponse response = webResource
                .path(ACCOUNT_1.getId().toString())
                .get(ClientResponse.class);

        assertThat(response.getStatus()).isEqualTo(200);

        Account actual = response.getEntity(new GenericType<Account>() {});

        assertMatchAccount(actual, ACCOUNT_1);
    }

    @Test
    public void save() throws Exception {
        WebResource webResource = client
                .resource(server.getURI())
                .path("account");

        Map<String,Object> postBody = new HashMap<>();
        postBody.put("amount", 100);
        postBody.put("name", "new name");
        Account expected = new Account("new name", new BigDecimal("100"));
        ClientResponse response = webResource
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, postBody);

        assertThat(response.getStatus()).isEqualTo(200);

        Account actual = response.getEntity(new GenericType<Account>() {});
        expected.setId(actual.getId());

        assertMatchAccount(actual, expected);
    }

}