package ru.home.moneytransfer.web;

import com.google.inject.Singleton;
import ru.home.moneytransfer.util.exception.ApplicationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Handler for map application exception to response
 * @author Pospekhov S
 */

@Provider
@Singleton
public class ExceptionHandler implements
        ExceptionMapper<ApplicationException> {

    public Response toResponse(ApplicationException exception) {
        return Response
                .status(exception.getHttpStatus().getCode())
                .entity(exception.getMsg())
                .build();
    }

}
