package ru.home.moneytransfer.repository.inmemory;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

import static ru.home.moneytransfer.model.TestData.*;
import static ru.home.moneytransfer.repository.inmemory.StoreObjects.getClone;

/**
 * Util class for reinit store in tests
 * @author Pospekhov S
 */

public class TestStoreObject {
    public static void clear() {
        StoreObjects.setACCOUNTS(new HashMap<>());
        StoreObjects.setTRANSACTIONS(new HashMap<>());
        StoreObjects.setAccountId(new AtomicLong(1L));
        StoreObjects.setTransactionId(new AtomicLong(1L));
    }

    public static void initForTest() {

        StoreObjects.ACCOUNTS.put(ACCOUNT_1.getId(), new Pair<>(getClone(ACCOUNT_1), new Semaphore(1)));
        StoreObjects.ACCOUNTS.put(ACCOUNT_2.getId(), new Pair<>(getClone(ACCOUNT_2), new Semaphore(1)));

        StoreObjects.TRANSACTIONS.put(TRANSACTION_1.getId(), getClone(TRANSACTION_1));
        StoreObjects.TRANSACTIONS.put(TRANSACTION_2.getId(), getClone(TRANSACTION_2));
    }
}
