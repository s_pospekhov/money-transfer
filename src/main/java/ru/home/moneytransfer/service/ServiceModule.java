package ru.home.moneytransfer.service;

import com.google.inject.AbstractModule;
import ru.home.moneytransfer.service.impl.AccountServiceImpl;
import ru.home.moneytransfer.service.impl.TransactionServiceImpl;

/**
 * Service classes binding
 * @author Pospekhov S
 */

public class ServiceModule extends AbstractModule {
    protected void configure() {
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(TransactionService.class).to(TransactionServiceImpl.class);
    }
}
