package ru.home.moneytransfer.repository.inmemory;

import ru.home.moneytransfer.model.Transaction;
import ru.home.moneytransfer.repository.TransactionRepository;
import ru.home.moneytransfer.util.exception.NotFoundException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *  Implementation for local repository for transaction
 *
 *  @author Pospekhov S
 */

public class TransactionalRepositoryLocal implements TransactionRepository {
    @Override
    public Transaction save(Transaction transaction) {
        if (Objects.isNull(transaction.getId())) {
            transaction.setId(StoreObjects.TRANSACTION_ID.getAndIncrement());
            StoreObjects.TRANSACTIONS.put(transaction.getId(), transaction);
            setTransactionToAccount(transaction);
            return transaction;
        } else {
            StoreObjects.TRANSACTIONS.put(transaction.getId(), transaction);
            setTransactionToAccount(transaction);
            return transaction;
        }
    }

    private void setTransactionToAccount(Transaction transaction) {
        StoreObjects.ACCOUNTS.get(transaction.getFromAccountId()).getKey().getTransactionsFrom().add(transaction);
        StoreObjects.ACCOUNTS.get(transaction.getToAccountId()).getKey().getTransactionsTo().add(transaction);
    }

    @Override
    public List<Transaction> getAll() {
        return StoreObjects.TRANSACTIONS.values().stream().collect(Collectors.toList());
    }

    @Override
    public Transaction get(Long id) {
        if (!StoreObjects.TRANSACTIONS.containsKey(id)) {
            throw new NotFoundException("Аккаунт не найден, id: " + id);
        }
        return StoreObjects.TRANSACTIONS.get(id);
    }
}
