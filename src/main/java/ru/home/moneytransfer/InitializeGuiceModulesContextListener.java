package ru.home.moneytransfer;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import ru.home.moneytransfer.repository.RepositoryModule;
import ru.home.moneytransfer.service.ServiceModule;
import ru.home.moneytransfer.util.PropertiesModule;
import ru.home.moneytransfer.web.AppServletModule;

/**
 * Create injector form application
 * @author Pospekhov S
 */

public class InitializeGuiceModulesContextListener extends GuiceServletContextListener {
    protected Injector getInjector() {
        return Guice.createInjector(new ServiceModule(), new AppServletModule(), new RepositoryModule(), new PropertiesModule());
    }
}
