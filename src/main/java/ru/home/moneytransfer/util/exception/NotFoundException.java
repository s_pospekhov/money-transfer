package ru.home.moneytransfer.util.exception;

import org.eclipse.jetty.http.HttpStatus;

/**
 *  Not found object in repository exception. HttpStatus.NOT_FOUND.
 *
 *  @author Pospekhov S
 */

public class NotFoundException extends ApplicationException {

    public NotFoundException(String msg) {
        super(msg, HttpStatus.Code.NOT_FOUND);
    }
}
