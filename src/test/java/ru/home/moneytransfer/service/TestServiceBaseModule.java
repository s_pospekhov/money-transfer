package ru.home.moneytransfer.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import ru.home.moneytransfer.repository.RepositoryModule;
import ru.home.moneytransfer.repository.inmemory.TestStoreObject;
import ru.home.moneytransfer.util.PropertiesModule;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Base class for service test. Init guice injector.
 * @author Pospekhov S
 */

public class TestServiceBaseModule {
    private Injector injector = Guice.createInjector(new ServiceModule(),
            new RepositoryModule(),
            new PropertiesModule()
    );

    public Injector getInjector() {
        return injector;
    }

    @Before
    public void setUp() throws IOException, URISyntaxException {
        injector.injectMembers(this);
        TestStoreObject.clear();
        TestStoreObject.initForTest();
    }
}
