package ru.home.moneytransfer;

import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.model.Transaction;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Util class for comparing objects
 * @author Pospekhov S
 */

public class TestUtil {
    public static void assertMatchAccount(Account actual, Account expected) {
        assertThat(actual).isEqualToIgnoringGivenFields(expected, "transactionsFrom", "transactionsTo", "amount");
        assertThat(actual.getAmount().compareTo(expected.getAmount())).isEqualTo(0);
        assertMatchTransaction(actual.getTransactionsFrom(), expected.getTransactionsFrom());
        assertMatchTransaction(actual.getTransactionsTo(), expected.getTransactionsTo());
    }

    public static void assertMatchAccount(List<Account> actual, List<Account> expected) {
        if ((Objects.isNull(actual))) {
            assertThat(expected).isNull();
        }
        assertThat(actual.size()).isEqualTo(expected.size());
        List<Account> actualList = actual.stream()
                .sorted(Comparator.comparing(Account::getId))
                .collect(Collectors.toList());
        List<Account> expectedList = expected.stream()
                .sorted(Comparator.comparing(Account::getId))
                .collect(Collectors.toList());

        for (int i = 0; i < actualList.size(); i++) {
            assertMatchAccount(actualList.get(i), expectedList.get(i));
        }
    }

    public static void assertMatchTransaction(Transaction actual, Transaction expected) {
        assertThat(actual).isEqualToIgnoringGivenFields(expected, "amount");
        assertThat(actual.getAmount().compareTo(expected.getAmount())).isEqualTo(0);
    }

    public static void assertMatchTransaction(List<Transaction> actual, List<Transaction> expected) {
        if ((Objects.isNull(actual))) {
            assertThat(expected).isNull();
        }
        assertThat(actual.size()).isEqualTo(expected.size());
        List<Transaction> actualList = actual.stream()
                .sorted(Comparator.comparing(Transaction::getId))
                .collect(Collectors.toList());
        List<Transaction> expectedList = expected.stream()
                .sorted(Comparator.comparing(Transaction::getId))
                .collect(Collectors.toList());

        for (int i = 0; i < actualList.size(); i++) {
            assertMatchTransaction(actualList.get(i), expectedList.get(i));
        }
    }
}
