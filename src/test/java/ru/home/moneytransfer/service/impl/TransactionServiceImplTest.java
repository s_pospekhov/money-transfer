package ru.home.moneytransfer.service.impl;

import com.google.inject.Inject;
import org.junit.Test;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.model.Transaction;
import ru.home.moneytransfer.service.AccountService;
import ru.home.moneytransfer.service.TestServiceBaseModule;
import ru.home.moneytransfer.service.TransactionService;
import ru.home.moneytransfer.util.exception.NotEnoughMoney;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.home.moneytransfer.TestUtil.assertMatchTransaction;
import static ru.home.moneytransfer.model.TestData.*;

/**
 * Transaction service test
 * @author Pospekhov S
 */

public class TransactionServiceImplTest extends TestServiceBaseModule {

    @Inject
    private TransactionService transactionService;
    @Inject
    private AccountService accountService;
    @Test
    public void get() throws Exception {
        Transaction actual = transactionService.get(TRANSACTION_1.getId());

        assertMatchTransaction(actual, TRANSACTION_1);
    }

    @Test
    public void save() throws Exception {
        Account accountTo = accountService.get(ACCOUNT_1.getId());
        Account accountFrom = accountService.get(ACCOUNT_2.getId());
        Transaction expected = new Transaction(null, "transaction message", new BigDecimal("200.00"), ACCOUNT_2.getId(), ACCOUNT_1.getId(), null);
        Transaction saved = transactionService.save(expected);
        expected.setId(saved.getId());
        expected.setSuccess(true);

        Transaction actual = transactionService.get(saved.getId());
        assertMatchTransaction(actual, expected);

        assertThat(accountTo.getTransactionsTo().stream().map(Transaction::getId).collect(Collectors.toList())).contains(expected.getId());
        assertThat(accountFrom.getTransactionsFrom().stream().map(Transaction::getId).collect(Collectors.toList())).contains(expected.getId());
    }

    @Test(expected = NotEnoughMoney.class)
    public void saveWithError() throws Exception {
        Transaction expected = new Transaction(null, "transaction message", new BigDecimal("2200.00"), ACCOUNT_2.getId(), ACCOUNT_1.getId(), null);
        transactionService.save(expected);
    }


    /**
     * Method test concurrent save some transaction for same account
     * @throws Exception wait ExecutorService end work exception
     */
    @Test
    public void saveWithThreads() throws Exception {
        Account account3 = accountService.save(new Account("name3", new BigDecimal("100.00")));
        Account account4 = accountService.save(new Account("name4", new BigDecimal("100.00")));

        ExecutorService threadPool = Executors.newFixedThreadPool(10);

        threadPool.execute(() -> {
            Transaction expected = new Transaction(null, "transaction message 1", new BigDecimal("400.00"), ACCOUNT_1.getId(), ACCOUNT_2.getId(), null);
            try {
                transactionService.save(expected);
            } catch (NotEnoughMoney e) {

            }
        });
        threadPool.execute(() -> {
            Transaction expected = new Transaction(null, "transaction message 2", new BigDecimal("400.00"), ACCOUNT_1.getId(), account3.getId(), null);
            try {
                transactionService.save(expected);
            } catch (NotEnoughMoney e) {

            }
        });
        threadPool.execute(() -> {
            Transaction expected = new Transaction(null, "transaction message 3", new BigDecimal("400.00"), ACCOUNT_1.getId(), account4.getId(), null);
            try {
                transactionService.save(expected);
            } catch (NotEnoughMoney e) {

            }
        });
        threadPool.shutdown();
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        assertThat(accountService.get(ACCOUNT_1.getId()).getAmount().compareTo(new BigDecimal("200"))).isEqualTo(0);

        assertThat(transactionService.getAll().stream()
                .filter(transaction -> !transaction.getSuccess())
                .count()).isEqualTo(1);
    }

    /**
     * Method test concurrent save some transaction for same account. Check deadlock
     * @throws Exception wait ExecutorService end work exception
     */
    @Test
    public void saveWithThreadsForLock() throws Exception {

        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        AtomicInteger count = new AtomicInteger(1);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(() -> {
                Transaction expected = new Transaction(null, "transaction " + count.getAndIncrement(), new BigDecimal("100.00"), ACCOUNT_1.getId(), ACCOUNT_2.getId(), null);
                System.out.println(expected.getMessage() + " started");
                try {
                    transactionService.save(expected);
                } catch (NotEnoughMoney e) {

                }
            });
            threadPool.execute(() -> {
                Transaction expected = new Transaction(null, "transaction " + count.getAndIncrement(), new BigDecimal("100.00"), ACCOUNT_2.getId(), ACCOUNT_1.getId(), null);
                System.out.println(expected.getMessage() + " started");
                try {
                    transactionService.save(expected);
                } catch (NotEnoughMoney e) {

                }
            });
        }

        threadPool.shutdown();
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        assertThat(accountService.get(ACCOUNT_1.getId()).getAmount().compareTo(new BigDecimal("1000"))).isEqualTo(0);
        assertThat(accountService.get(ACCOUNT_2.getId()).getAmount().compareTo(new BigDecimal("2000"))).isEqualTo(0);

        assertThat(transactionService.getAll().size()).isEqualTo(42);

        assertThat(transactionService.getAll().stream()
                .filter(transaction -> !transaction.getSuccess())
                .count()).isEqualTo(0);
    }



}