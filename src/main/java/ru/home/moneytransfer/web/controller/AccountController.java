package ru.home.moneytransfer.web.controller;

import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.service.AccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Controller class for account
 * @author Pospekhov S
 */

@Path("/account")
@RequestScoped
public class AccountController {
    private final AccountService accountService;

    @Inject
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account get(@PathParam("id") Long id) {
        return accountService.get(id);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Account save(Account account) {
        return accountService.save(account);
    }

}
