package ru.home.moneytransfer.util.exception;


import org.eclipse.jetty.http.HttpStatus;

/**
 *  Not enough money exception. HttpStatus.CONFLICT.
 *
 *  @author Pospekhov S
 */

public class NotEnoughMoney extends ApplicationException {

    public NotEnoughMoney(String msg) {
        super(msg, HttpStatus.Code.CONFLICT);
    }
}
