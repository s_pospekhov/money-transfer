package ru.home.moneytransfer.repository.inmemory;

import javafx.util.Pair;
import org.eclipse.jetty.http.HttpStatus;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.repository.AccountRepository;
import ru.home.moneytransfer.util.exception.ApplicationException;
import ru.home.moneytransfer.util.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

/**
 *  Implementation for local repository for account
 *
 *  @author Pospekhov S
 */

public class AccountRepositoryLocal implements AccountRepository {

    @Override
    public Account get(Long id) {
        if (!StoreObjects.ACCOUNTS.containsKey(id)) {
            throw new NotFoundException("Аккаунт не найден, id " + id);
        }
        return StoreObjects.ACCOUNTS.get(id).getKey();
    }

    @Override
    public Account getWithLock(Long id) {
        if (!StoreObjects.ACCOUNTS.containsKey(id)) {
            throw new NotFoundException("Аккаунт не найден, id " + id);
        }
        try {
            StoreObjects.ACCOUNTS.get(id).getValue().acquire();
        } catch (InterruptedException e) {
            throw new ApplicationException("Ошибка при попытке заблокировать объект для работы.", HttpStatus.Code.INTERNAL_SERVER_ERROR);
        }
        return StoreObjects.ACCOUNTS.get(id).getKey();
    }

    @Override
    public Account save(Account account) {
        if (Objects.isNull(account.getId())) {
            account.setId(StoreObjects.ACCOUNT_ID.getAndIncrement());
            if (Objects.isNull(account.getTransactionsTo())) {
                account.setTransactionsTo(new ArrayList<>());
            }
            if (Objects.isNull(account.getTransactionsFrom())) {
                account.setTransactionsFrom(new ArrayList<>());
            }
            StoreObjects.ACCOUNTS.put(account.getId(), new Pair<>(account, new Semaphore(1)));
            return account;
        } else {
            StoreObjects.ACCOUNTS.put(account.getId(), new Pair<>(account, StoreObjects.ACCOUNTS.get(account.getId()).getValue()));
            StoreObjects.ACCOUNTS.get(account.getId()).getValue().release();
            return account;
        }
    }

    @Override
    public List<Account> getAll() {
        return StoreObjects.ACCOUNTS.values().stream().map(Pair::getKey).collect(Collectors.toList());
    }
}
