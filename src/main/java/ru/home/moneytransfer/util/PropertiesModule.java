package ru.home.moneytransfer.util;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Binding properties from application.properties
 * @author Pospekhov S
 */

public class PropertiesModule extends AbstractModule {
    @Override
    protected void configure() {
        loadProperties(binder());
    }

    private void loadProperties(Binder binder) {
        InputStream stream =
                PropertiesModule.class.getResourceAsStream("/application.properties");
        Properties appProperties = new Properties();
        try {
            appProperties.load(stream);
            Names.bindProperties(binder, appProperties);
        } catch (IOException e) {
            binder.addError(e);
        }
    }


}
