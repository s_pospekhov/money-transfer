package ru.home.moneytransfer.repository;

import ru.home.moneytransfer.model.Transaction;

import java.util.List;

/**
 *  Repository for transactions
 *
 *  @author Pospekhov S
 */

public interface TransactionRepository {
    /**
     * Save object in repository
     * @param transaction Object for save
     * @return Saved transaction object
     */
    Transaction save(Transaction transaction);

    /**
     * Get all objects from repository
     * @return List transaction objects
     */
    List<Transaction> getAll();

    /**
     * Get object
     * @param id transaction unique id
     * @return Transaction object
     */
    Transaction get(Long id);

}
