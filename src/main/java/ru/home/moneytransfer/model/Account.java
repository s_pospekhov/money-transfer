package ru.home.moneytransfer.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *  Model class for user account
 *
 *  @author Pospekhov S
 */

public class Account {

    private Long id;

    private String name;

    private BigDecimal amount;

    private List<Transaction> transactionsFrom;

    private List<Transaction> transactionsTo;

    public Account() {
    }

    public Account(String name, BigDecimal amount) {
        this.name = name;
        this.amount = amount;
        this.transactionsFrom = new ArrayList<>();
        this.transactionsTo = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public List<Transaction> getTransactionsFrom() {
        return transactionsFrom;
    }

    public void setTransactionsFrom(List<Transaction> transactionsFrom) {
        this.transactionsFrom = transactionsFrom;
    }

    public List<Transaction> getTransactionsTo() {
        return transactionsTo;
    }

    public void setTransactionsTo(List<Transaction> transactionsTo) {
        this.transactionsTo = transactionsTo;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", transactionsFrom=" + transactionsFrom +
                ", transactionsTo=" + transactionsTo +
                '}';
    }

}
