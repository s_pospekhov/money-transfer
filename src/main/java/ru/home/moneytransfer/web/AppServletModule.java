package ru.home.moneytransfer.web;

import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

/**
 * Controller url binding
 * @author Pospekhov S
 */

public class AppServletModule extends ServletModule {
    @Override
    protected void configureServlets() {
        PackagesResourceConfig resourceConfig = new PackagesResourceConfig("ru.home.moneytransfer.web.controller");
        for (Class<?> resource : resourceConfig.getClasses()) {
            bind(resource);
        }
        serve("/*").with(GuiceContainer.class);

        bind(ExceptionHandler.class);
    }
}
