package ru.home.moneytransfer.service.impl;

import com.google.inject.Inject;
import ru.home.moneytransfer.model.Account;
import ru.home.moneytransfer.model.Transaction;
import ru.home.moneytransfer.repository.AccountRepository;
import ru.home.moneytransfer.repository.TransactionRepository;
import ru.home.moneytransfer.service.TransactionService;
import ru.home.moneytransfer.util.exception.NotEnoughMoney;

import java.util.List;

/**
 *  Implementation for service for transaction
 *
 *  @author Pospekhov S
 */

public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    @Inject
    public TransactionServiceImpl(AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public Transaction get(Long id) {
        return transactionRepository.get(id);
    }

    public Transaction save(Transaction transaction) {
        Account from;
        Account to;
        if (transaction.getFromAccountId() > transaction.getToAccountId()) {
            from = accountRepository.getWithLock(transaction.getFromAccountId());
            to = accountRepository.getWithLock(transaction.getToAccountId());
        } else {
            to = accountRepository.getWithLock(transaction.getToAccountId());
            from = accountRepository.getWithLock(transaction.getFromAccountId());
        }

        if (from.getAmount().compareTo(transaction.getAmount()) < 0) {
            transaction.setSuccess(false);
            transactionRepository.save(transaction);

            throw new NotEnoughMoney("У вас недостаточно средств для этого перевода, аккаунт id: " + transaction.getFromAccountId());
        }
        from.setAmount(from.getAmount().subtract(transaction.getAmount()));
        accountRepository.save(from);
        to.setAmount(to.getAmount().add(transaction.getAmount()));
        accountRepository.save(to);
        Transaction result = transactionRepository.save(transaction);
        result.setSuccess(true);
        result = transactionRepository.save(result);
        return result;
    }

    public List<Transaction> getAll() {
        return transactionRepository.getAll();
    }
}
